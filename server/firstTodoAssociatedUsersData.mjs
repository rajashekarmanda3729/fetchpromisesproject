import fetch from "node-fetch"

fetch("https://jsonplaceholder.typicode.com/todos?id=1")
    .then((response) => {
        return response.json()
    }).then((todo) => {
        return Promise.all([todo,fetch("https://jsonplaceholder.typicode.com/users")])
    }).then(([todo,res]) => {
        return Promise.all([todo,res.json()])
    }).then(([todo,users]) => {
        return firstTodoUsers([todo,users])
    }).catch((err) => {
        console.log(err)
    })

function firstTodoUsers([todo,users]) {
    let firstTodoMatchedUsersData = []
    const { userId } = todo[0]
    users.map(user => (userId === user.id) ? firstTodoMatchedUsersData.push({ [userId]: user }) : '')
    console.log(firstTodoMatchedUsersData)
}
