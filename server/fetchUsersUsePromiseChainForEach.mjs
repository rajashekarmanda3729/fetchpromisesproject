import fetch from "node-fetch"

fetch("https://jsonplaceholder.typicode.com/users")
    .then((users) => {
        return users.json()
    }).then((users) => {
        return Promise.all([fetch("https://jsonplaceholder.typicode.com/todos"),users])
    }).then(([todos,users]) => {
        return Promise.all([todos.json(),users])
    }).then(([todos,users]) => {
        return getDetailsUsers([todos,users])
    }).catch((err) => {
        console.log(err)
    })
    
function getDetailsUsers([todos,users]){
    let eachuserData = []
    users.map(eachUser =>{
        let eachUserDetails = []
        todos.map(eachTodo => {
            if (eachUser.id === eachTodo.userId){
                eachUserDetails.push(eachTodo)
            }
        })
        eachuserData.push({[eachUser.name]:eachUserDetails})
    })
    console.log(eachuserData)
}