import fetch from "node-fetch"

fetch("https://jsonplaceholder.typicode.com/users")
    .then((users) => {
        return users.json()
    }).then((users) => {
        return Promise.all([users, fetch("https://jsonplaceholder.typicode.com/todos")])
    }).then(([users, todos]) => {
        return Promise.all([users, todos.json()])
    }).then(([users, todos]) => {
        console.log(todos)
        console.log(users)
    }).catch((err) => {
        console.log(err)
    })

